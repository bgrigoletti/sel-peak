<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function getAllAlunos(){

        $data = array();

        $sql = "
            SELECT a.id_aluno, a.nome, a.data_nascimento, c.nome AS curso FROM aluno AS a
            LEFT JOIN curso AS c ON c.id_curso = a.id_curso
            ORDER BY a.nome ASC";
        $query = $this->db->query($sql);

        foreach ($query->result() as $row){
            $data[] = $row;
        }

        return $data;

    }


    public function aluno_info($id = NULL){

        $data = array();

        $sql = "SELECT * FROM aluno WHERE id_aluno = ?";
        $query = $this->db->query($sql, array($id));

        foreach ($query->result() as $row) {
            $data = $row;
        }

        return $data;

    }


    public function insert_aluno(array $data){

        if($this->db->insert('aluno', $data)){
            return TRUE;
        }

        return FALSE;

    }


    public function update_aluno($id, array $data){

        $aluno = $this->aluno_info($id);

        if(!empty($aluno->id_aluno)){
            if($this->db->update('aluno', $data, array('id_aluno' => $aluno->id_aluno))){
                return TRUE;
            }
        }

        return FALSE;

    }


    public function delete_aluno($id = NULL){

        if($this->db->delete('aluno', array('id_aluno' => $id))){
            return TRUE;
        }

        return FALSE;

    }


}
