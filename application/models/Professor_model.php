<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professor_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function getAllProfessores(){

        $data = array();

        $sql = "SELECT * FROM professor ORDER BY nome ASC";
        $query = $this->db->query($sql);

        foreach ($query->result() as $row){
            $data[] = $row;
        }

        return $data;

    }


    public function checkProfessorCurso($id = NULL){

        $data = array();

        if($id == NULL){

            $sql = "SELECT id_professor FROM curso GROUP BY id_professor";
            $query = $this->db->query($sql);

            foreach ($query->result() as $row){

                $data[$row->id_professor] = array(
                    'id_professor' => $row->id_professor,
                );

            }
        }
        else{

            $data['check'] = FALSE;

            $sql = "SELECT 1 FROM curso WHERE id_professor = ?";
            $query = $this->db->query($sql, array($id));

            foreach ($query->result() as $row){
                $data['check'] = TRUE;
            }

        }

        return $data;

    }


    public function professor_info($id = NULL){

        $data = array();

        $sql = "SELECT * FROM professor WHERE id_professor = ?";
        $query = $this->db->query($sql, array($id));

        foreach ($query->result() as $row) {
            $data = $row;
        }

        return $data;

    }


    public function insert_professor(array $data){

        if($this->db->insert('professor', $data)){
            return TRUE;
        }
        
        return FALSE;

    }


    public function update_professor($id, array $data){

        $professor = $this->professor_info($id);

        if(!empty($professor->id_professor)){
            if($this->db->update('professor', $data, array('id_professor' => $professor->id_professor))){
                return TRUE;
            }

        }

        return FALSE;

    }


    public function delete_professor($id = NULL){

        if($this->db->delete('professor', array('id_professor' => $id))){
            return TRUE;
        }

        return FALSE;

    }


}
