<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function getInfosRelatorio(){

        $data = array();

        $sql = "
            SELECT a.nome AS aluno, c.nome AS curso, p.nome AS professor FROM aluno AS a
            LEFT JOIN curso AS c ON c.id_curso = a.id_curso
            LEFT JOIN professor AS p ON p.id_professor = c.id_professor
            ORDER BY a.nome ASC";

        $query = $this->db->query($sql);

        foreach ($query->result() as $row){
            $data[] = $row;
        }

        return $data;

    }

}
