<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function getAllCursos(){

        $data = array();

        $sql = "
            SELECT c.id_curso, c.nome, p.nome AS professor FROM curso AS c
            LEFT JOIN professor AS p ON c.id_professor = p.id_professor
            ORDER BY c.nome ASC";
        $query = $this->db->query($sql);

        foreach ($query->result() as $row){
            $data[] = $row;
        }

        return $data;

    }


    public function checkCursoAluno($id = NULL){

        $data = array();

        if($id == NULL){

            $sql = "SELECT id_curso FROM aluno GROUP BY id_curso";
            $query = $this->db->query($sql);

            foreach ($query->result() as $row){

                $data[$row->id_curso] = array(
                    'id_curso' => $row->id_curso,
                );

            }
        }
        else{

            $data['check'] = FALSE;

            $sql = "SELECT 1 FROM aluno WHERE id_curso = ?";
            $query = $this->db->query($sql, array($id));

            foreach ($query->result() as $row){
                $data['check'] = TRUE;
            }

        }

        return $data;

    }


    public function curso_info($id = NULL){

        $data = array();

        $sql = "SELECT * FROM curso WHERE id_curso = ?";
        $query = $this->db->query($sql, array($id));

        foreach ($query->result() as $row) {
            $data = $row;
        }

        return $data;

    }


    public function insert_curso(array $data){

        if($this->db->insert('curso', $data)){
            return TRUE;
        }

        return FALSE;

    }


    public function update_curso($id, array $data){

        $curso = $this->curso_info($id);

        if(!empty($curso->id_curso)){
            if($this->db->update('curso', $data, array('id_curso' => $curso->id_curso))){
                return TRUE;
            }
        }

        return FALSE;

    }


    public function delete_curso($id = NULL){

        if($this->db->delete('curso', array('id_curso' => $id))){
            return TRUE;
        }

        return FALSE;

    }


}
