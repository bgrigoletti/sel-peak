<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo $title; ?></h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('professor/create', '<i class="fa fa-plus"></i> Cadastrar professor', array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">

                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Data de Nascimento</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($professores as $prof):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($prof->nome, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo date("d/m/Y", strtotime(str_replace('-','/',$prof->data_nascimento))); ?></td>
                                                <td>
                                                    <?php echo anchor('professor/edit/'.$prof->id_professor, '<span class="label label-warning">Editar</span>'); ?>&nbsp;
                                                    <?php if(!isset($checkProfessor[$prof->id_professor])): ?>
                                                        <?php echo anchor('professor/delete/'.$prof->id_professor, '<span class="label label-danger">Excluir</span>'); ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
