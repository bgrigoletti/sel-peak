<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
            <!--
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b></b>
                </div>
            </footer>
            -->
        </div>

        <script src="<?php echo base_url('assets/frameworks/jquery/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>"></script>
        <script src="<?php echo base_url('assets/frameworks/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/slimscroll/slimscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/frameworks/adminlte/js/adminlte.min.js'); ?>"></script>

        <script>
            console.log(navigator.userAgent);
            $("#data_nascimento").inputmask("date", {
                inputFormat: "dd/mm/yyyy",
                outputFormat: "mm-yyyy-dd",
                inputEventOnly: true
            });

        </script>


        <?php if($title == "Alunos"): ?>


            <script type="text/javascript">

                $(document).ready(function() {

                    function limpa_formulário_cep() {
                        // Limpa valores do formulário de cep.
                        $("#rua").val("");
                        $("#bairro").val("");
                        $("#cidade").val("");
                        $("#uf").val("");
                        $("#ibge").val("");
                    }
                    
                    //Quando o campo cep perde o foco.
                    $("#cep").blur(function() {

                        //Nova variável "cep" somente com dígitos.
                        var cep = $(this).val().replace(/\D/g, '');

                        //Verifica se campo cep possui valor informado.
                        if (cep != "") {

                            //Expressão regular para validar o CEP.
                            var validacep = /^[0-9]{8}$/;

                            //Valida o formato do CEP.
                            if(validacep.test(cep)) {

                                $("#enviar").attr("disabled", true);

                                //Preenche os campos com "..." enquanto consulta webservice.
                                $("#rua").val("...");
                                $("#bairro").val("...");
                                $("#cidade").val("...");
                                $("#uf").val("...");

                                //Consulta o webservice viacep.com.br/
                                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                                    if (!("erro" in dados)) {
                                        //Atualiza os campos com os valores da consulta.
                                        $("#rua").val(dados.logradouro);
                                        $("#bairro").val(dados.bairro);
                                        $("#cidade").val(dados.localidade);
                                        $("#uf").val(dados.uf);

                                        $("#enviar").attr("disabled", false);

                                    } //end if.
                                    else {
                                        //CEP pesquisado não foi encontrado.
                                        limpa_formulário_cep();
                                        alert("CEP não encontrado.");

                                        $("#enviar").attr("disabled", false);

                                    }
                                });
                            } //end if.
                            else {
                                //cep é inválido.
                                limpa_formulário_cep();
                                alert("Formato de CEP inválido.");
                            }
                        } //end if.
                        else {
                            //cep sem valor, limpa formulário.
                            limpa_formulário_cep();
                            $("#enviar").attr("disabled", true);

                        }
                    });
                });

            </script>


        <?php endif; ?>
    </body>
</html>