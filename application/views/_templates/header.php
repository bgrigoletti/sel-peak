<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <meta name="HandheldFriendly" content="true">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="google" content="notranslate">
        <meta name="robots" content="follow">
        <link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqElEQVRYR+2WYQ6AIAiF8W7cq7oXd6v5I2eYAw2nbfivYq+vtwcUgB1EPPNbRBR4Tby2qivErYRvaEnPAdyB5AAi7gCwvSUeAA4iis/TkcKl1csBHu3HQXg7KgBUegVA7UW9AJKeA6znQKULoDcDkt46bahdHtZ1Por/54B2xmuz0uwA3wFfd0Y3gDTjhzvgANMdkGb8yAyY/ro1d4H2y7R1DuAOTHfgAn2CtjCe07uwAAAAAElFTkSuQmCC">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,700italic">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frameworks/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frameworks/font-awesome/css/font-awesome.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frameworks/ionicons/css/ionicons.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frameworks/adminlte/css/adminlte.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frameworks/adminlte/css/skins/skin-blue.min.css'); ?>">

        <!--[if lt IE 9]>
            <script src="<?php echo base_url('/assets/plugins/html5shiv/html5shiv.min.js'); ?>"></script>
            <script src="<?php echo base_url('/assets/plugins/respond/respond.min.js'); ?>"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
        <div class="wrapper">


            <header class="main-header">
                <a href="<?php echo site_url('dashboard'); ?>" class="logo">
                    <span class="logo-mini"><b>SL</b></span>
                    <span class="logo-lg"><b>Seleção Peak</b></span>
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                    <!-- Sidebar menu -->
                    <ul class="sidebar-menu">


                        <li class="header text-uppercase">Navegação Principal</li>

                        <li>
                            <a href="<?php echo site_url('dashboard'); ?>">
                                <i class="fa fa-dashboard"></i><span>Painel principal</span>
                            </a>
                        </li>

                        <li class="header text-uppercase">Opções</li>

                        <li>
                            <a href="<?php echo site_url('professor'); ?>">
                                <i class="fa fa-user"></i> <span>Professores</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('curso'); ?>">
                                <i class="fa fa-tasks"></i> <span>Cursos</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('aluno'); ?>">
                                <i class="fa fa-user"></i> <span>Alunos</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('relatorio'); ?>" target="_blank">
                                <i class="fa fa-file-pdf-o"></i> <span>Relatório PDF</span>
                            </a>
                        </li>

                    </ul>
                </section>
            </aside>