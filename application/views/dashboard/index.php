<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

        <div class="content-wrapper">
            <section class="content-header">
                <?php echo $title; ?>
            </section>

            <section class="content">
                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Alunos cadastrados</span>
                                <span class="info-box-number"><?php echo $count_alunos; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Professores cadastrados</span>
                                <span class="info-box-number"><?php echo $count_professores; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-list"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Cursos cadastradas</span>
                                <span class="info-box-number"><?php echo $count_cursos; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>





