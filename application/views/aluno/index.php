<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo $title; ?></h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('aluno/create', '<i class="fa fa-plus"></i> Cadastrar aluno', array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">

                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Data de Nascimento</th>
                                                <th>Curso</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($alunos as $ln):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($ln->nome, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo date("d/m/Y", strtotime(str_replace('-','/',$ln->data_nascimento))); ?></td>
                                                <td><?php echo htmlspecialchars($ln->curso, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td>
                                                    <?php echo anchor('aluno/edit/'.$ln->id_aluno, '<span class="label label-warning">Editar</span>'); ?>&nbsp;
                                                    <?php echo anchor('aluno/delete/'.$ln->id_aluno, '<span class="label label-danger">Excluir</span>'); ?>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
