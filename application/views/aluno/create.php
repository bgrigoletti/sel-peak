<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo $title; ?></h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Cadastrar aluno</h3>
                                </div>
                                <div class="box-body">
                                    <?php echo $message;?>

                                    <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit_user')); ?>

                                        <div class="form-group">
                                            <label for="nome" class="col-sm-2 control-label">Nome</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($nome);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="data_nascimento" class="col-sm-2 control-label">Data de Nascimento</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($data_nascimento);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="data_nascimento" class="col-sm-2 control-label">Curso</label>
                                            <div class="col-sm-10">
                                                <?php echo form_dropdown('curso', $curso, $select_curso, array('class' => 'form-control')); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="cep" class="col-sm-2 control-label">CEP</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($cep);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="rua" class="col-sm-2 control-label">Rua</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($rua);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="numero" class="col-sm-2 control-label">Número</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($numero);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="bairro" class="col-sm-2 control-label">Bairro</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($bairro);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="cidade" class="col-sm-2 control-label">Cidade</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($cidade);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="uf" class="col-sm-2 control-label">Estado</label>
                                            <div class="col-sm-10">
                                                <?php echo form_input($uf);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'submit', 'id' => 'enviar', 'class' => 'btn btn-primary btn-flat', 'content' => 'Enviar')); ?>
                                                    <?php echo anchor('aluno', 'Cancelar', array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
