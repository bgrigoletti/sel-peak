<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo $title; ?></h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('curso/create', '<i class="fa fa-plus"></i> Cadastrar curso', array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">

                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Professor</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($cursos as $crs):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($crs->nome, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($crs->professor, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td>
                                                    <?php echo anchor('curso/edit/'.$crs->id_curso, '<span class="label label-warning">Editar</span>'); ?>&nbsp;
                                                    <?php if(!isset($checkCurso[$crs->id_curso])): ?>
                                                        <?php echo anchor('curso/delete/'.$crs->id_curso, '<span class="label label-danger">Excluir</span>'); ?>
                                                    <?php endif; ?>                                                    
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
