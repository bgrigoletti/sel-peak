<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professor extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('professor_model','modProfessor',true);

        $this->data['title'] = 'Professores';
    }


	public function index()
	{

        $this->data['professores'] = $this->modProfessor->getAllProfessores();

        $this->data['checkProfessor'] = $this->modProfessor->checkProfessorCurso(NULL);

        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('professor/index', $this->data);
        $this->load->view('_templates/footer', $this->data);
	}


    public function create(){

        /* Validate form input */
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('data_nascimento', 'Data de Nascimento', 'required|valid_date');
        
        if ($this->form_validation->run() == TRUE){

            $data = array(
                'nome' => $this->input->post('nome'),
                'data_nascimento' => date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('data_nascimento')))),
            );

            if($this->modProfessor->insert_professor($data)){
                redirect('professor', 'refresh');
            }
            else {
                redirect('/', 'refresh');
            }

        }
        else
        {
            $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

            $this->data['nome'] = array(
                'name'  => 'nome',
                'id'    => 'nome',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nome'),
            );

            $this->data['data_nascimento'] = array(
                'name'  => 'data_nascimento',
                'id'    => 'data_nascimento',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('data_nascimento'),
            );

            /* Load Template */
            $this->load->view('_templates/header', $this->data);
            $this->load->view('professor/create', $this->data);
            $this->load->view('_templates/footer', $this->data);

        }
    }


    public function edit($id= NULL){

        $id = (int) $id;

        /* Data */
        $professor = $this->modProfessor->professor_info($id);

        if(empty($professor->id_professor)){
            redirect('professor', 'refresh');
        }

        /* Validate form input */
        if (isset($_POST) && !empty($_POST)){

            /* Validate form input */
            $this->form_validation->set_rules('nome', 'Nome', 'required');
            $this->form_validation->set_rules('data_nascimento', 'Data de Nascimento', 'required|valid_date');

            if ($this->form_validation->run() == TRUE){

                $data = array(
                    'nome' => $this->input->post('nome'),
                    'data_nascimento' => date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('data_nascimento')))),
                );

                if($this->modProfessor->update_professor($professor->id_professor, $data)){
                    redirect('professor', 'refresh');
                }
                else{
                    redirect('/', 'refresh');
                }
            }
        }


        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

        // pass the user to the view
        $this->data['professor']  = $professor;

        $this->data['nome'] = array(
            'name'  => 'nome',
            'id'    => 'nome',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nome', $professor->nome)
        );

        $this->data['data_nascimento'] = array(
            'name'  => 'data_nascimento',
            'id'    => 'data_nascimento',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('data_nascimento', date("d/m/Y", strtotime(str_replace('-','/', $professor->data_nascimento))))
        );

        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('professor/edit', $this->data);
        $this->load->view('_templates/footer', $this->data);

    }


    public function delete($id = NULL){

        /* Validate form input */
        $this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
        $this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

        $id = (int) $id;

        if ($this->form_validation->run() === FALSE)
        {

            $professor = $this->modProfessor->professor_info($id);

            $checkProfessor = $this->modProfessor->checkProfessorCurso($id);

            if(empty($professor->id_professor) || $checkProfessor['check']){
                redirect('professor', 'refresh');
            }

            $this->data['id']         = (int) $professor->id_professor;
            $this->data['professor']  = ! empty($professor->nome) ? htmlspecialchars($professor->nome, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->load->view('_templates/header', $this->data);
            $this->load->view('professor/delete', $this->data);
            $this->load->view('_templates/footer', $this->data);

        }
        else
        {
            if ($this->input->post('confirm') == 'yes'){
                $this->modProfessor->delete_professor($id);
            }

            redirect('professor', 'refresh');
        }
    }


}