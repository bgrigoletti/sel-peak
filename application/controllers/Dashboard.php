<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model','modDash',true);
    }


	public function index()
	{

        /* Title Page */
        $this->data['title'] = 'Dashboard';

        /* Data */
        $this->data['count_alunos']      = $this->modDash->get_count_record('aluno');
        $this->data['count_professores'] = $this->modDash->get_count_record('professor');
        $this->data['count_cursos']      = $this->modDash->get_count_record('curso');

        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('dashboard/index', $this->data);
        $this->load->view('_templates/footer', $this->data);
	}
}

