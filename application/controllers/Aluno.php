<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('professor_model','modProfessor',true);
        $this->load->model('curso_model','modCurso',true);
        $this->load->model('aluno_model','modAluno',true);

        $this->data['title'] = 'Alunos';
    }


	public function index()
	{

        $this->data['alunos'] = $this->modAluno->getAllAlunos();

        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('aluno/index', $this->data);
        $this->load->view('_templates/footer', $this->data);
	}


    public function create(){

        /* Validate form input */
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('data_nascimento', 'Data de Nascimento', 'required|valid_date');
        $this->form_validation->set_rules('curso', 'Curso', 'required');
        $this->form_validation->set_rules('cep', 'CEP', 'required');
        $this->form_validation->set_rules('rua', 'Rua', 'required');
        $this->form_validation->set_rules('numero', 'Número', 'required');
        $this->form_validation->set_rules('bairro', 'Bairro', 'required');
        $this->form_validation->set_rules('cidade', 'Cidade', 'required');
        $this->form_validation->set_rules('uf', 'Estado', 'required');

        if ($this->form_validation->run() == TRUE){

            $data = array(
                'nome' => $this->input->post('nome'),
                'data_nascimento' => date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('data_nascimento')))),
                'logradouro' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'bairro' => $this->input->post('bairro'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('uf'),
                'cep' => $this->input->post('cep'),
                'id_curso' => $this->input->post('curso'),
            );

            if($this->modAluno->insert_aluno($data)){
                redirect('aluno', 'refresh');
            }
            else {
                redirect('/', 'refresh');
            }

        }
        else
        {

            $this->data['curso'][''] = 'selecione um curso';

            $lista_curso = $this->modCurso->getAllCursos();

            foreach($lista_curso as $curso){
                $this->data['curso'][$curso->id_curso] = $curso->nome;
            }

            $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

            $this->data['nome'] = array(
                'name'  => 'nome',
                'id'    => 'nome',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nome'),
            );

            $this->data['data_nascimento'] = array(
                'name'  => 'data_nascimento',
                'id'    => 'data_nascimento',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('data_nascimento'),
            );

            $this->data['cep'] = array(
                'name'  => 'cep',
                'id'    => 'cep',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('cep'),
            );

            $this->data['rua'] = array(
                'name'  => 'rua',
                'id'    => 'rua',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('rua'),
                'readonly' => 'readonly',
            );

            $this->data['numero'] = array(
                'name'  => 'numero',
                'id'    => 'numero',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('numero'),
            );

            $this->data['bairro'] = array(
                'name'  => 'bairro',
                'id'    => 'bairro',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('bairro'),
                'readonly' => 'readonly',
            );

            $this->data['cidade'] = array(
                'name'  => 'cidade',
                'id'    => 'cidade',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('cidade'),
                'readonly' => 'readonly',
            );

            $this->data['uf'] = array(
                'name'  => 'uf',
                'id'    => 'uf',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('uf'),
                'maxlength' => '2',
                'readonly' => 'readonly',
            );


            $this->data['select_curso'] = (!empty($_POST['curso'])) ? $this->input->post('curso') : '';

            /* Load Template */
            $this->load->view('_templates/header', $this->data);
            $this->load->view('aluno/create', $this->data);
            $this->load->view('_templates/footer', $this->data);

        }
    }


    public function edit($id= NULL){

        $id = (int) $id;

        /* Data */
        $aluno = $this->modAluno->aluno_info($id);

        if(empty($aluno->id_aluno)){
            redirect('aluno', 'refresh');
        }

        /* Validate form input */
        if (isset($_POST) && !empty($_POST)){

            /* Validate form input */
            $this->form_validation->set_rules('nome', 'Nome', 'required');
            $this->form_validation->set_rules('data_nascimento', 'Data de Nascimento', 'required|valid_date');
            $this->form_validation->set_rules('curso', 'Curso', 'required');
            $this->form_validation->set_rules('cep', 'CEP', 'required');
            $this->form_validation->set_rules('rua', 'Rua', 'required');
            $this->form_validation->set_rules('numero', 'Número', 'required');
            $this->form_validation->set_rules('bairro', 'Bairro', 'required');
            $this->form_validation->set_rules('cidade', 'Cidade', 'required');
            $this->form_validation->set_rules('uf', 'Estado', 'required');

            if ($this->form_validation->run() == TRUE){

                $data = array(
                    'nome' => $this->input->post('nome'),
                    'data_nascimento' => date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('data_nascimento')))),
                    'logradouro' => $this->input->post('rua'),
                    'numero' => $this->input->post('numero'),
                    'bairro' => $this->input->post('bairro'),
                    'cidade' => $this->input->post('cidade'),
                    'estado' => $this->input->post('uf'),
                    'cep' => $this->input->post('cep'),
                    'id_curso' => $this->input->post('curso'),
                );

                if($this->modAluno->update_aluno($aluno->id_aluno, $data)){
                    redirect('aluno', 'refresh');
                }
                else{
                    redirect('/', 'refresh');
                }
            }
        }


        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

        // pass the user to the view
        $this->data['aluno']  = $aluno;

        $this->data['nome'] = array(
            'name'  => 'nome',
            'id'    => 'nome',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nome', $aluno->nome)
        );

        $this->data['data_nascimento'] = array(
            'name'  => 'data_nascimento',
            'id'    => 'data_nascimento',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('data_nascimento', date("d/m/Y", strtotime(str_replace('-','/', $aluno->data_nascimento))))
        );

        $this->data['cep'] = array(
            'name'  => 'cep',
            'id'    => 'cep',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('cep', $aluno->cep)
        );

        $this->data['rua'] = array(
            'name'  => 'rua',
            'id'    => 'rua',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('rua', $aluno->logradouro),
            'readonly' => 'readonly',
        );

        $this->data['numero'] = array(
            'name'  => 'numero',
            'id'    => 'numero',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('numero', $aluno->numero),
        );

        $this->data['bairro'] = array(
            'name'  => 'bairro',
            'id'    => 'bairro',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('bairro', $aluno->bairro),
            'readonly' => 'readonly',
        );

        $this->data['cidade'] = array(
            'name'  => 'cidade',
            'id'    => 'cidade',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('cidade', $aluno->cidade),
            'readonly' => 'readonly',
        );

        $this->data['uf'] = array(
            'name'  => 'uf',
            'id'    => 'uf',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('uf', $aluno->estado),
            'maxlength' => '2',
            'readonly' => 'readonly',
        );

        $lista_cursos = $this->modCurso->getAllCursos();

        foreach($lista_cursos as $curso){
            $this->data['curso'][$curso->id_curso] = $curso->nome;
        }

        $this->data['select_curso'] = (!empty($_POST['curso'])) ? $this->input->post('curso') : $curso->id_curso;


        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('aluno/edit', $this->data);
        $this->load->view('_templates/footer', $this->data);

    }


    public function delete($id = NULL){

        /* Validate form input */
        $this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
        $this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

        $id = (int) $id;

        if ($this->form_validation->run() === FALSE)
        {

            $aluno = $this->modAluno->aluno_info($id);

            if(empty($aluno->id_aluno)){
                redirect('aluno', 'refresh');
            }

            $this->data['id']         = (int) $aluno->id_aluno;
            $this->data['aluno']  = ! empty($aluno->nome) ? htmlspecialchars($aluno->nome, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->load->view('_templates/header', $this->data);
            $this->load->view('aluno/delete', $this->data);
            $this->load->view('_templates/footer', $this->data);

        }
        else
        {
            if ($this->input->post('confirm') == 'yes'){
                $this->modAluno->delete_aluno($id);
            }

            redirect('aluno', 'refresh');
        }
    }


}