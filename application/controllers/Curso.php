<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('professor_model','modProfessor',true);
        $this->load->model('curso_model','modCurso',true);

        $this->data['title'] = 'Cursos';
    }


	public function index()
	{

        $this->data['cursos'] = $this->modCurso->getAllCursos();

        $this->data['checkCurso'] = $this->modCurso->checkCursoAluno(NULL);

        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('curso/index', $this->data);
        $this->load->view('_templates/footer', $this->data);
	}


    public function create(){

        /* Validate form input */
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('professor', 'Professor', 'required');
        
        if ($this->form_validation->run() == TRUE){

            $data = array(
                'nome' => $this->input->post('nome'),
                'id_professor' => $this->input->post('professor'),
            );

            if($this->modCurso->insert_curso($data)){
                redirect('curso', 'refresh');
            }
            else {
                redirect('/', 'refresh');
            }

        }
        else
        {

            $this->data['professor'][''] = 'selecione um professor';

            $lista_professores = $this->modProfessor->getAllProfessores();

            foreach($lista_professores as $professor){
                $this->data['professor'][$professor->id_professor] = $professor->nome;
            }

            $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

            $this->data['nome'] = array(
                'name'  => 'nome',
                'id'    => 'nome',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nome'),
            );

            $this->data['select_professor'] = (!empty($_POST['professor'])) ? $this->input->post('professor') : '';

            /* Load Template */
            $this->load->view('_templates/header', $this->data);
            $this->load->view('curso/create', $this->data);
            $this->load->view('_templates/footer', $this->data);

        }
    }


    public function edit($id= NULL){

        $id = (int) $id;

        /* Data */
        $curso = $this->modCurso->curso_info($id);

        if(empty($curso->id_curso)){
            redirect('curso', 'refresh');
        }

        /* Validate form input */
        if (isset($_POST) && !empty($_POST)){

            /* Validate form input */
            $this->form_validation->set_rules('nome', 'Nome', 'required');
            $this->form_validation->set_rules('professor', 'Professor', 'required');

            if ($this->form_validation->run() == TRUE){

                $data = array(
                    'nome' => $this->input->post('nome'),
                    'id_professor' => $this->input->post('professor'),
                );

                if($this->modCurso->update_curso($curso->id_curso, $data)){
                    redirect('curso', 'refresh');
                }
                else{
                    redirect('/', 'refresh');
                }
            }
        }


        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

        // pass the user to the view
        $this->data['curso']  = $curso;

        $this->data['nome'] = array(
            'name'  => 'nome',
            'id'    => 'nome',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('nome', $curso->nome)
        );


        $lista_professores = $this->modProfessor->getAllProfessores();

        foreach($lista_professores as $professor){
            $this->data['professor'][$professor->id_professor] = $professor->nome;
        }

        $this->data['select_professor'] = (!empty($_POST['professor'])) ? $this->input->post('professor') : $curso->id_professor;


        /* Load Template */
        $this->load->view('_templates/header', $this->data);
        $this->load->view('curso/edit', $this->data);
        $this->load->view('_templates/footer', $this->data);

    }


    public function delete($id = NULL){

        /* Validate form input */
        $this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
        $this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

        $id = (int) $id;

        if ($this->form_validation->run() === FALSE)
        {

            $curso = $this->modCurso->curso_info($id);

            $checkCurso = $this->modCurso->checkCursoAluno($id);

            if(empty($curso->id_curso) || $checkCurso['check']){
                redirect('curso', 'refresh');
            }

            $this->data['id']         = (int) $curso->id_curso;
            $this->data['curso']  = ! empty($curso->nome) ? htmlspecialchars($curso->nome, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->load->view('_templates/header', $this->data);
            $this->load->view('curso/delete', $this->data);
            $this->load->view('_templates/footer', $this->data);

        }
        else
        {
            if ($this->input->post('confirm') == 'yes'){
                $this->modCurso->delete_curso($id);
            }

            redirect('curso', 'refresh');
        }
    }


}