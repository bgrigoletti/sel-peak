<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('relatorio_model','modRelatorio',true);
        $this->load->library('pdf');

    }


	public function index()
	{

        /* Data */
        $lista = $this->modRelatorio->getInfosRelatorio();

        $output = '<h3 align="center">Relatório PDF</h3>';

        $output .= '<table width="100%" cellspacing="5" cellpadding="5">';

        $output .= '
        <tr>
            <td><b>Aluno</b></td>
            <td><b>Professor</b></td>
            <td><b>Curso</b></td>
        </tr>
        ';

        foreach($lista as $row){

            $output .= '
            <tr>
                <td>'.$row->aluno.'</td>
                <td>'.$row->professor.'</td>
                <td>'.$row->curso.'</td>
            </tr>
            ';
        }

        $output .= '</table>';

        $this->pdf->loadHtml($output);
        $this->pdf->render();
        $this->pdf->stream("teste.pdf", array("Attachment"=>0));

	}
}

