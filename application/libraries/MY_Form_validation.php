<?php

class MY_Form_validation extends CI_Form_validation {

	public function __construct($rules = array())
	{
	    parent::__construct($rules);
	}


	public function valid_date($date)
	{

		$parte = explode('/', $date);

		if(count($parte) == 3){

			foreach($parte as $row){
				if(!is_numeric($row)){
					return false;
				}
			}

			$date = date("Y-m-d",strtotime(str_replace('/','-',$date)));


		    $d = DateTime::createFromFormat('Y-m-d', $date);
		    return $d && $d->format('Y-m-d') === $date;

		}

		return false;

	}
}