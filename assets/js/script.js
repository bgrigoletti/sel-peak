
    $(document).ready(function(){

        $(".categoria").change(function() {

            if($(".categoria").val() == 'NULL'){
                $(".lista-estados").css("display","none");
                $(".lista-itens").css("display","none");
            }
            else{

                $(".lista-itens").css("display","none");

                var post_data = {
                    'pagina' : $(".selecao").attr('data-page'),
                    'categoria' : $(".categoria").val(),
                    'estado' : '',
                    '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    url: $(".selecao").attr('data-url')+'json/lista',
                    type: 'POST',
                    data: post_data,
                    dataType: 'json',
                    success: function(msg){

                        $('option', '#estados').remove();

                        $('#estados').append($('<option>', {
                            value: 'NULL',
                            text : 'selecione o estado'
                        }));

                        $.each(msg, function(i, item) {
                            $('#estados').append($('<option>', {
                                value: item.id,
                                text : item.estado
                            }));

                        });

                        $(".lista-estados").css("display","block");

                    }
                });

            }

            return false;

        });


        $(".estados").change(function() {

            if($(".categoria").val() == 'NULL'){
                $(".lista-itens").css("display","none");
            }
            else{

                var post_data = {
                    'pagina' : $(".selecao").attr('data-page'),
                    'categoria' : $(".categoria").val(),
                    'estado' : $(".estados").val(),
                    '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    url: $(".selecao").attr('data-url')+'json/lista',
                    type: 'POST',
                    data: post_data,
                    dataType: 'json',
                    success: function(msg){

                        $('.lista-itens').empty();

                        $.each(msg, function(i, item) {
                            /*$('.lista-itens').append('<div class="item"><div class="foto">'+item.imagem+'</div><p class="local">'+item.cidade+'</p><p class="nome">'+item.titulo+'</p><p>Endere&ccedil;o: '+item.endereco+'</p><p>Fone: '+item.telefone+'</p></div>');*/

                            conteudo = '<div class="item">';
                            conteudo += (item.cidade != '')   ? '<p class="local">'+item.cidade+'</p>' : '';
                            conteudo += (item.nome != '')     ? '<p class="nome">'+item.titulo+'</p>' : '';
                            conteudo += (item.endereco != '') ? '<p>Endere&ccedil;o: '+item.endereco+'</p>' : '';
                            conteudo += (item.telefone != '') ? '<p>Fone: '+item.telefone+'</p>' : '';
                            conteudo += (item.email != '')    ? '<p>E-mail: '+item.email+'</p>' : '';
                            conteudo += '</div><hr />';

                            $('.lista-itens').append(conteudo);
                        });

                        $(".lista-itens").css("display","block");

                    }
                });

            }

            return false;

        });



        if ($('#group_bgcolor').length) {
            var elem = $('#group_bgcolor');

            elem.ColorPickerSliders({
                size: 'lg',
                placement: 'auto bottom',
                previewformat: 'hex',
                color: elem.attr('data-src'),
                swatches: ['#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#009688', '#FF5722', '#795548', '#607D8B', '#000000'],
                customswatches: false,
                order: {}
            });

            $('button[type="reset"]').on('click', function(e){
                elem.trigger('colorpickersliders.updateColor', elem.attr('data-src'));
            });
        }


    });